import { bootstrap }    from '@angular/platform-browser-dynamic';
import 'rxjs/Rx';
import {AppComponent} from './app.component'
import {enableProdMode} from '@angular/core';
enableProdMode();
bootstrap(AppComponent);
