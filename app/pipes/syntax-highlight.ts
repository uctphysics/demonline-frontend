import { Pipe, PipeTransform } from '@angular/core';
declare var hljs: any;

@Pipe({
  name: 'syntaxHighlight'
})
export class SyntaxHighlightPipe implements PipeTransform  {
  transform(content: string, lang: string) {
    return hljs.highlight(lang, content).value;
  }
}

