export interface DemoEntry {
    title : string;
    reference: string;
    category: string;
    purpose?: string;
    description?: string;
    image?: string;
    img?: string;
    author?: string;
    script?: string;
    similarTo?: string[];
}
