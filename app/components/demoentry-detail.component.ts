import { Component, OnInit} from '@angular/core';
import {NgClass} from '@angular/common';
import { Router, RouteParams } from '@angular/router-deprecated';
import {DemoEntry} from '../models/demoentry'
import {Category} from '../models/category'
import {DemoEntryService} from '../services/demoentry.service';
import { CategoryService } from '../services/category.service';
import { SyntaxHighlightPipe } from '../pipes/syntax-highlight';

declare var Clipboard: any;


@Component({
    selector: 'my-entry-detail',
    templateUrl: 'app/components/templates/demoentry-detail.component.html',
    styleUrls: ['app/components/templates/stylesheets/demoentry-detail.component.css'],
    inputs: ['entry'],
    directives: [NgClass],
    pipes: [ SyntaxHighlightPipe ]
})
export class DemoEntryDetailComponent implements OnInit {
    entry: DemoEntry;    
    category: Category;
    errorMessage: string;
    highlightedScript: string;
    imgZoom: boolean = false;
               
    constructor(
            private _demoEntryService: DemoEntryService,
            private _categoryService: CategoryService,
            private _router: Router,
            private _routeParams: RouteParams) {}
    ngOnInit() {
        let ref = this._routeParams.get('ref');
        this._demoEntryService.getEntry(ref)
              .subscribe(
                    entry => this.entry = entry,
                    error =>  this.errorMessage = <any>error,
                    () => this._categoryService.getCategory(this.entry.category).subscribe(
                        category => this.category = category,
                        error =>  this.errorMessage = <any>error
              ));
    }
    
    toggleImgZoom() {
        this.imgZoom = !this.imgZoom;
    }
    
    copyScript(){
        var clipboardDemos=new Clipboard('[data-clipboard-demo]');
    }
    gotoEntry(ref: string) {
        let link = ['DemoEntryDetail', { ref: ref }];
        this._router.navigate(link);
    }
    
    gotoList(category: Category) {
        let link = ['Category', { prefix: category.prefix }];
        this._router.navigate(link);
    }
    
    goHome(){
        let link = ['Dashboard'];
        this._router.navigate(link);
    }
    
    goBack() {
        window.history.back();
    }
}