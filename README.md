# DemOnline V2 (Frontend) #

demonline2 is a MEAN stack rewrite of a collection of (hundreds of )static pages, acting as a database of all available lecture demonstration equipment in the UCT Physics department. V2 
supports searching through the database, as well as content management, when logged in as an admin user. The frontend is written in typescript using the Angular2 framework. 

The website is accessible here: http://webapp-phy.uct.ac.za/demonline2/
### Technologies used ###

* typescript
* Angular2
* bootstrap CSS